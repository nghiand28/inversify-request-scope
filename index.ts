import "reflect-metadata";
import * as bodyParser from 'body-parser';

import { Container } from 'inversify';
import { interfaces, InversifyExpressServer, TYPE } from 'inversify-express-utils';

// declare metadata by @controller annotation
import "./test";
import { Service, TracingMiddleware, TYPES } from "./test";

// set up container
let container = new Container();

// set up bindings
container.bind<Service>(TYPES.Service).to(Service).inTransientScope();
container.bind<TracingMiddleware>(TYPES.TracingMiddleware).to(TracingMiddleware);
container.bind<string>(TYPES.TraceId).toConstantValue("test");

// create server
let server = new InversifyExpressServer(container);
server.setConfig((app) => {
    // add body parser
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
});

let app = server.build();
app.listen(3000);
