import { inject, injectable } from "inversify";
import { BaseHttpController, BaseMiddleware, controller, httpGet } from "inversify-express-utils";
import * as express from "express";

export const TYPES = {
  TraceId: Symbol.for("TraceId"),
  TracingMiddleware: Symbol.for("TracingMiddleware"),
  Service: Symbol.for("Service"),
};

@injectable()
export class TracingMiddleware extends BaseMiddleware {

  public async handler(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    this.bind<string>(TYPES.TraceId)
      .toConstantValue(`${req.header('X-Trace-Id') || '123'}`);

    next();
  }
}

@controller("/")
export class TracingTestController extends BaseHttpController {

  constructor(@inject(TYPES.Service) private readonly service: Service) {
    super();
  }

  @httpGet(
    "/",
    TYPES.TracingMiddleware
  )
  public getTest() {
    return this.service.doSomethingThatRequiresTheTraceID();
  }
}

@injectable()
export class Service {
  private uuid: string;
  constructor(
    @inject(TYPES.TraceId) private readonly traceID: string
  ) {
    this.uuid = (Math.random() * 2 ** 64).toString();
  }

  public doSomethingThatRequiresTheTraceID() {
    return `${this.traceID} + ${this.uuid}`;
  }
}